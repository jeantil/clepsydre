(function(window) {
var config = { queue: [] };
var Test = function(name, callback) {
	this.name = name;
	this.callback = callback;
	this.assertions = [];
};

Test.prototype = {
	setup: function() {
		config.current = this;
	},
	
	run: function() {
		try {
			this.callback.call();
		} catch(e) {
	    console.error( 'Test ' + this.testName + ' died: ' + e );
			SUnit.ok( false, 'Died on test ' + (this.assertions.length + 1) + ': ' + e.message );
		}
	},
	
	finish: function() {
		var bad = 0, msg = '', tests = id('tests'), len = this.assertions.length;
		config.stats.all += len;
		for ( var i = 0 ; i < len ; i++ ) {
			if ( ! this.assertions[i].result ) {
				bad++;
				config.stats.bad++;
				var m = this.assertions[i].message;
				if (m) msg = m;
			}
		}

		var li = document.createElement('li');
		li.style.color = bad ? 'red'   : 'green';
		li.innerHTML = ( bad ? 'FAIL ' : 'PASS ' ) + this.name + msg ;		
		tests.appendChild( li );
	},

	queue: function() {
		var test = this;
  	config.queue.push( function() { test.setup(); } );
  	config.queue.push( function() { test.run();   } );
  	config.queue.push( function() { test.finish();} );
	},
	
	process: function() {
	  while ( config.queue.length ) {
		  if ( config.updateRate ) {
			  config.queue.shift()();
		  } else {
			  window.setTimeout( this.process, 13 );
			  break;
		  }
	  }
    if ( !config.queue.length) { // done
      var runtime = +new Date - config.started,
		    passed = config.stats.all - config.stats.bad;

      id( 'testresult' ).innerHTML = [
			    'Tests completed in ', runtime, 'ms. ',
			    passed, ' tests of ',	config.stats.all,	' passed, ',
			    config.stats.bad, ' failed.'
		    ].join('');
    }
  }
};

var SUnit = {
	config: {},
	init: function() {
		config = {
			stats: { all: 0, bad: 0 },
			started: +new Date,
			updateRate: 1000,
			queue: [] // The queue of tests to run
		};

		var tests = id( 'tests' ), result;
		result = document.createElement( 'p' );
		result.id = 'testresult';
		tests.parentNode.insertBefore( result, tests );
	},

	test: function(testName, callback) {
		var test = new Test(testName, callback);
		test.queue();
		test.process();
	},

	ok: function(a, msg) {
		a = !!a;
		config.current.assertions.push({
			result: a,
			message: msg
		});
	},

	equal: function(actual, expected) {
		SUnit.push(expected == actual, actual, expected);
	},

	push: function(result, actual, expected) {
		var output;
		if (actual != expected) output = ' Expected ' + expected +' but was ' + actual ;
		config.current.assertions.push({ result: !!result, message: output });
	}
};

window.addEventListener( 'load', function() {
  var ol = document.createElement('ol');
  ol.id = 'tests';
  document.body.appendChild( ol );
	// Initialize the config, saving the execution queue
	var oldconfig = extend({}, config);
	SUnit.init();
	extend(config, oldconfig);
}, false );

extend(window, SUnit); // Expose the API as global variables

function extend(a, b) {
	for ( var prop in b ) a[prop] = b[prop];
	return a;
}

function id(name) {
	return document.getElementById( name );
}
})(this);
