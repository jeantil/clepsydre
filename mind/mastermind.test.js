test("One good guess.", function() {
    equal( mastermind('B','B'), '1,0' );
});
test("No good guess.", function() {
    equal( mastermind('B','M'), '0,0' );
});
test("One good guess in two.", function() {
    equal( mastermind('B,R','B,M'), '1,0' );
});
test("One misplaced in two.", function() {
    equal( mastermind('M,R','B,M'), '0,1' );
});
test("Two misplaced, two good guess.", function() {
    equal( mastermind('B,M,B,B','B,B,B,M'), '2,2' );
});
