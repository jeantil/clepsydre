function mastermind( secret, guess ) {
  var result,
    good=0,
    bad=0,
    badcolors=[];
    misplaced=false,
    badsecret=[],
    badguess=[];
  
  for ( var i = 0; i < secret.length; i++ ) {
    if ( secret[i] == guess[i] ) {
      good++
    } else {
      badsecret.push(secret[i]);
      badguess.push(guess[i]);
    }
  }
  for ( var i = 0; i < badsecret.length; i++ ) {
    if (badguess.indexOf(badsecret[i]) != -1 && badcolors.indexOf(badsecret[i]) == -1) {
      var nbbadguess=0;
      for ( var j = 0; j < badguess.length; j++ ) {
        if (badguess[j] == badsecret[i]) {
          nbbadguess++;
        }
      }
      var nbbadsecret=0;
      for ( var k = 0; k < badsecret.length; k++ ) {
        if (badsecret[k] == badsecret[i]) {
          nbbadsecret++;
        }
      }
      bad += Math.min(nbbadguess,nbbadsecret);
      badcolors.push(badsecret[i]);
    }
  }
  result = [good,bad].toString();
  return result;
}
