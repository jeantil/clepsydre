function mastermind( secret, guess ) {
  
	function same( elem, item ) { return( secret[item] == guess[item] ); }
	var good = secret.filter( same ).length;
	
	function diff( elem, item ) { return( secret[item] != guess[item] ); }
	var bad=0, badcolors=[],
		badguess = guess.filter( diff ),
		badsecret = secret.filter( diff );
	
	function countbad( color ) {
    if ( badguess.contains( color ) && !badcolors.contains( color ) ) {
			function bycolor( elem ) { return( elem == color ); }
      bad += Math.min(
				badguess.filter( bycolor ).length, 
				badsecret.filter( bycolor ).length
			);
      badcolors.push( color );
    }
	}
	badsecret.filter( countbad );
	
  return [good,bad].toString();
}

Array.prototype.contains = function( obj ) { var i = this.length; while ( i-- ) { if ( this[i] === obj ) { return true; } } return false;}
