function toSeconds(string){
	var minutesSeconds = string.split(":");
	var seconds = 0;
	if(minutesSeconds.length>1){
		seconds = 60*Number(minutesSeconds.shift());
	}
	seconds += Number(minutesSeconds.shift());
	return seconds;
}
function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
function fromSeconds(number){
	var minutes = Math.floor(number / 60);
	var seconds = number % 60;
	return (minutes ? ""+minutes+":" : "")+pad(seconds,2);
}
