function handler (req, res) {  
  var contentTypesByExtension = {
      '.html' : "text/html",
      '.js' : "text/javascript",
      '.css' : "text/css",
      '.jpg' : "image/jpeg",
      '.svg' : "image/svg+xml",
      '.less' : "text/css"
  };
  var filePath = url.parse(req.url).pathname;

  if (filePath == '/'){
      filePath = '/index.html';
  }
  filePath=__dirname + filePath
  fileExtension = path.extname(filePath);
  fs.readFile(filePath,
  function (err, data) {
    if (err) {
      console.error("err",err.toString());
      res.writeHead(404);
      return res.end(err.toString());
    }
    var contentType = contentTypesByExtension[fileExtension] || 'text/plain';   
    res.writeHead(200, { 'Content-Type': contentType });
    res.end(data, 'utf-8');
  });
}

var  app = require('http').createServer(handler)
  , io = require('socket.io').listen(app)
  , url = require('url')
  , path = require('path')
  , fs = require('fs');
app.listen(8000);
var clepsydre = {
  devs: {},
  client: {
    story: "",
    test: "",
    backlog: []
  },
  current_test:"",
  master:{    
    src:"headcrash.png"
  }
};

io.sockets.on('connection', function (client) {
  console.log('new connection'+client.id);

  client.on('digest', function (hash) {
    var dev = {
      clientid:client.id,
      digest:hash,
      log: [{
          code: "init",
          test: "init",
          result: "NONE"
        }]
    }
    clepsydre.devs[client.id]=dev;
    io.sockets.emit('update', clepsydre);    
  });
  client.on('newstory', function (data) {
    console.log('newstory accepted+'+data.data);
    clepsydre.current_test=data.data;
    io.sockets.emit('story', data.data);
  });
  client.on('newhint', function (data) {
    io.sockets.emit('hint', data.data);
  });
  client.on('fire', function (data) {
    clepsydre.devs={};
    io.sockets.emit('update', clepsydre);
  });
  client.on('master_code_update', function (data) {
    io.sockets.emit('master_code_update', data);
  });
  client.on('commit', function (data) {
    console.log('commit code:'+data.code);
    clepsydre.devs[client.id].log.push({
      code: data.code,
      test: data.test,
      result: data.result
    });
    io.sockets.emit('update', clepsydre);
    io.sockets.emit('commit', data);
    if(data.result){
      io.sockets.emit('code2review', data.code, client.id);      
    }
  });
  client.on('turn_clepsydre', function (data) {
    io.sockets.emit('turn_clepsydre', data);
  });
  client.on('master_connection',function(data){
    clepsydre.master.id=client.id;
    clepsydre.master.src="http://www.gravatar.com/avatar/" + data + "?s=29";
    io.sockets.emit('master_connection',clepsydre.master.src);
  });
  client.on('disconnect', function () {
    console.log("CLIENT DISCONNECT", client.id);
    delete clepsydre.devs[client.id];
    if(client.id==clepsydre.master.id){
      clepsydre.master={src:"headcrash.png"};
    }
    io.sockets.emit('update', clepsydre);
  });
});
